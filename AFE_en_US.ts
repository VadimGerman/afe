<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>ArticleWidget</name>
    <message>
        <location filename="articlewidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="articlewidget.ui" line="50"/>
        <source>Title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="articlewidget.ui" line="80"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="articlewidget.ui" line="107"/>
        <source>Category</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="articlewidget.ui" line="126"/>
        <source>Wrong layout</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="20"/>
        <source>MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="33"/>
        <source>Site info:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="45"/>
        <source>NewsMax</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="58"/>
        <source>Name*:</source>
        <oldsource>Name:</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="71"/>
        <source>Link*:</source>
        <oldsource>Link:</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="84"/>
        <source>https://www.newsmax.com</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="97"/>
        <source>Recent news sub URL*:</source>
        <oldsource>Recent news sub URL:</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="110"/>
        <source>/newsfront</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="123"/>
        <source>Description:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="136"/>
        <source>Independent. American.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="149"/>
        <source>Language*:</source>
        <oldsource>Language:</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="162"/>
        <source>EN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="176"/>
        <source>Parsing:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="188"/>
        <source>a.article_link_bold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="201"/>
        <source>Article link*:</source>
        <oldsource>Article link:</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="214"/>
        <source>Title*:</source>
        <oldsource>Title:</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="227"/>
        <source>div[id=artPgHdLnWrapper]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="240"/>
        <source>Content block*:</source>
        <oldsource>Content block:</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="253"/>
        <source>div[id=artPgLeftWrapper]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="279"/>
        <source>Description block*:</source>
        <oldsource>Description block:</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="292"/>
        <source>div[id=mainArticleDiv] &gt; p</source>
        <oldsource>div[id=mainArticleDiv] \\u003e p</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="316"/>
        <source>Result:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="329"/>
        <source>Send</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="342"/>
        <source>Clean</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="384"/>
        <source>Article:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
