#pragma once

#include <QMainWindow>
#include <QtWebEngineWidgets>

#include "utils/networkmanager.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

enum MessageColor {
    Black,
    Green,
    Red,
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void sendClicked();
    void cleanClicked();
    void selectedChainged(int index);

private:
    void setStatusBarMessage(const QString &message, MessageColor color);

private:
    Ui::MainWindow *ui;
    QWebEngineView *m_view;

    QVariantList m_news;
    NetworkManager m_networkManager;
};
