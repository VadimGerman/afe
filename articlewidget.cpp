#include "articlewidget.h"
#include "ui_articlewidget.h"

ArticleWidget::ArticleWidget(const Article &article, QWidget *parent)
    : m_article(article),
      QWidget(parent),
      ui(new Ui::ArticleWidget)
{
    ui->setupUi(this);
    ui->title->setText(m_article.title);
    ui->description->setText(m_article.description);
    ui->category->setText(m_article.category);
}

ArticleWidget::~ArticleWidget()
{
    delete ui;
}
