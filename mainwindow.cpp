
#include <QJsonDocument>
#include <QDebug>

#include "structs/newsource.h"
#include "articlewidget.h"

#include "mainwindow.h"
#include "./ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setFixedSize(geometry().width(), geometry().height());

    QObject::connect(ui->sendButton, &QPushButton::clicked, this, &MainWindow::sendClicked);
    QObject::connect(ui->cleanButton, &QPushButton::clicked, this, &MainWindow::cleanClicked);
    QObject::connect(ui->newsView, &QListWidget::currentRowChanged, this, &MainWindow::selectedChainged);

    m_view = new QWebEngineView(ui->webFrame);
    m_view->resize(ui->webFrame->size());

    setStatusBarMessage("Ready.", Black);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::sendClicked()
{
    setStatusBarMessage("Waiting for server reply.", Black);

    QString url = ui->siteURL->text();
    url = url.endsWith('/') ? url.mid(0, url.length() - 1) : url;
    NewSource data(-1,
                   url,
                   ui->siteName->text(),
                   ui->siteDescription->text(),
                   url + "/favicon.ico",
                   ui->siteLanguage->text(),
                   ui->siteNewsURL->text(),
                   ui->articleLinkTag->text(),
                   ui->contentBlock->text(),
                   ui->title->text(),
                   "",
                   ui->descriptionBlock->text(),
                   {});

    auto onComplete = [this](const QJsonObject &json, const QList<QNetworkReply::RawHeaderPair> &){
        qInfo() << "parsing reply";

        QVariantMap data = json.toVariantMap();

        QVariantList feedsList = data.value("feeds").toList();
        if (feedsList.isEmpty()) {
            qWarning() << "Can not parse feeds!";
            ui->siteInfoField->setText("Can not parse feeds!");
            ui->siteInfoField->setTextColor(QColor(Qt::GlobalColor::red));
        } else {
            QJsonDocument document = QJsonDocument::fromVariant(feedsList.at(0).toMap());
            QByteArray byteArray = document.toJson(QJsonDocument::JsonFormat::Indented);
            ui->siteInfoField->setText(byteArray);
            ui->siteInfoField->setTextColor(QColor(Qt::GlobalColor::black));
        }

        m_news = data.value("news").toList();
        ui->newsView->clear();
        for (QVariant &item : m_news) {
            ArticleWidget *articleWidget = new ArticleWidget(Article::fromVariantMap(item.toMap()));
            QListWidgetItem *listItem = new QListWidgetItem();
            listItem->setSizeHint(QSize(0, 170));
            ui->newsView->addItem(listItem);
            ui->newsView->setItemWidget(listItem, articleWidget);
        }

        setStatusBarMessage("Check your data.", Black);
    };

    auto onError = [this](const QString &error){
        qWarning() << error;

        setStatusBarMessage(error, Red);
    };

    m_networkManager.sendRequest("/feeds", onComplete, onError, data.toVariantMap());
}

void MainWindow::cleanClicked()
{
    ui->siteURL->clear();
    ui->siteName->clear();
    ui->siteDescription->clear();
    ui->siteLanguage->clear();
    ui->siteNewsURL->clear();
    ui->articleLinkTag->clear();
    ui->contentBlock->clear();
    ui->title->clear();
    ui->descriptionBlock->clear();
    ui->siteInfoField->clear();
    ui->newsView->clear();
    m_news.clear();

    setStatusBarMessage("Ready.", Black);
}

void MainWindow::selectedChainged(int index)
{
    if (index < 0 || index >= m_news.size()) {
        m_view->setUrl(QUrl(""));
        return;
    }

    QVariant newsVariant = m_news.at(index);
    QVariantMap newsMap = newsVariant.toMap();
    QString link = newsMap.value("link").toString();

    qInfo() << "going to open:" << link;

    m_view->setUrl(QUrl(link));
}

void MainWindow::setStatusBarMessage(const QString &message, MessageColor color)
{
    ui->statusbar->showMessage(message);
    switch (color) {
    case MessageColor::Black:
        ui->statusbar->setStyleSheet("color: black");
        break;
    case MessageColor::Green:
        ui->statusbar->setStyleSheet("color: green");
        break;
    case MessageColor::Red:
        ui->statusbar->setStyleSheet("color: red");
        break;
    }
}
