#pragma once

#include <QWidget>

#include "structs/article.h"

namespace Ui {
class ArticleWidget;
}

class ArticleWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ArticleWidget(const Article &article, QWidget *parent = nullptr);
    ~ArticleWidget();

private:
    Ui::ArticleWidget *ui;

    Article m_article;
};
