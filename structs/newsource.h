#pragma oncem

#include <QVariantMap>
#include <QString>
#include <QList>

struct NewSource
{
    NewSource(int id,
              const QString &link,
              const QString &title,
              const QString &description,
              const QString &image,
              const QString &feedLanguage,
              const QString &subPath,
              const QString &articleLinkTag,
              const QString &contentBlock,
              const QString &articleTitle,
              const QString &category,
              const QString &descriptionBlock,
              const QList<QString> &forbiddenImgSubStrings);

    QVariantMap toVariantMap() const;

    int id;
    QString link;
    QString title;
    QString description;
    QString image;
    QString feedLanguage;

    QString subPath;
    QString articleLinkTag;
    QString contentBlock;
    QString articleTitle;
    QString category;
    QString descriptionBlock;

    QList<QString> forbiddenImgSubStrings;
};
