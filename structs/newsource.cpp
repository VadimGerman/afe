#include "newsource.h"

NewSource::NewSource(int id,
                     const QString &link,
                     const QString &title,
                     const QString &description,
                     const QString &image,
                     const QString &feedLanguage,
                     const QString &subPath,
                     const QString &articleLinkTag,
                     const QString &contentBlock,
                     const QString &articleTitle,
                     const QString &category,
                     const QString &descriptionBlock,
                     const QList<QString> &forbiddenImgSubStrings)
    : id(id),
      link(link),
      title(title),
      description(description),
      image(image),
      feedLanguage(feedLanguage),
      subPath(subPath),
      articleLinkTag(articleLinkTag),
      contentBlock(contentBlock),
      articleTitle(articleTitle),
      category(category),
      descriptionBlock(descriptionBlock),
      forbiddenImgSubStrings(forbiddenImgSubStrings)
{
}

QVariantMap NewSource::toVariantMap() const
{
    QVariantMap info;

    QVariantMap feed;
    feed.insert("ID", id);
    feed.insert("Link", link);
    feed.insert("Title", title);
    feed.insert("Description", description);
    feed.insert("Image", image);
    feed.insert("FeedLanguage", feedLanguage);

    QVariantMap subUrl;
    subUrl.insert("SubPath", subPath);
    subUrl.insert("ArticleLinkTag", articleLinkTag);
    QVariantList subUrlsList;
    subUrlsList.push_back(subUrl);

    QVariantMap tags;
    tags.insert("ContentBlock", contentBlock);
    tags.insert("Title", articleTitle);
    tags.insert("Category", category);
    tags.insert("DescriptionBlock", descriptionBlock);

    QVariantList fIS;
    for (const QString &element : forbiddenImgSubStrings)
        fIS << element;

    info.insert("Feed", feed);
    info.insert("ContentSubURLs", subUrlsList);
    info.insert("Tags", tags);
    info.insert("ForbiddenImgSubstrings", fIS);

    QVariantMap result;
    result.insert("Info", info);
    result.insert("ScrappedURLs", QVariant());

    return result;
}
