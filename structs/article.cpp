#include "article.h"

Article Article::fromVariantMap(const QVariantMap &map)
{
    Article result;
    result.title = map.value("title").toString();
    result.description = map.value("description").toString();
    result.category = map.value("category").toString();

    return result;
}

Article::Article()
{
}
