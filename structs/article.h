#pragma once

#include <QVariantMap>
#include <QString>

struct Article
{
    static Article fromVariantMap(const QVariantMap &map);

    Article();

    QString title;
    QString description;
    QString category;
};
