
#include <QtNetwork/QNetworkRequest>
#include <QJsonDocument>
#include <QJsonArray>
#include <QUrlQuery>
#include <QtGlobal>
#include <QDebug>

#include "networkmanager.h"

NetworkManager::NetworkManager()
{
    m_restclient = QSharedPointer<QNetworkAccessManager>::create();
    m_server = "http://localhost";
    m_port = 80;
}

QNetworkRequest NetworkManager::createRequest(const QString &apiStr) const
{
    QUrl url(m_server + ":" + QString::number(m_port) + apiStr);

    QNetworkRequest request;
    request.setUrl(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
    request.setRawHeader("Accept", "json");

    return request;
}

bool NetworkManager::isFinishedWithoutError(QNetworkReply *reply) const
{
    int code = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    if (code >= 200 && code < 300)
        return true;

    return false;
}

void NetworkManager::sendRequest(const QString &apiStr,
                                 const onCompleteFunc &onCompleteCallback,
                                 const onErrorFunc &onErrorCallback,
                                 const QVariantMap &data)
{
    QNetworkRequest request = createRequest(apiStr);
    QJsonDocument document = QJsonDocument::fromVariant(data);
    QByteArray byteArray = document.toJson(QJsonDocument::JsonFormat::Compact);
    byteArray = QString("json=" + byteArray).toLocal8Bit();
    qInfo() << QString::fromUtf8(byteArray.data()).toLatin1();

    QNetworkReply *reply = m_restclient->post(request, byteArray);

    connect(reply, &QNetworkReply::finished, this,
            [this, onCompleteCallback, onErrorCallback, reply]() {
        QJsonObject replyJson = parseReply(reply);
        QJsonDocument doc(replyJson);
        QString strJson = doc.toJson(QJsonDocument::Indented);
        qInfo() << "response:";
        qInfo().noquote() << QString::fromUtf8(strJson.toStdString().c_str());

        if (isFinishedWithoutError(reply)) {
            qInfo() << "request sended.";
            if (onCompleteCallback != nullptr) {
                QList<QNetworkReply::RawHeaderPair> headerPairs = reply->rawHeaderPairs();
                onCompleteCallback(replyJson, headerPairs);
            }
        } else {
            QString errorString;
            if (replyJson.empty())
                errorString = "timeout";
            else
                errorString = replyJson.value("message").toString();

            if (onErrorCallback != nullptr)
                onErrorCallback(errorString);
        }
        reply->close();
        reply->deleteLater();
    } );
}

QJsonObject NetworkManager::parseReply(QNetworkReply *reply) const
{
    QByteArray replyText = reply->readAll();
    QJsonParseError parseError;
    QJsonDocument jsonDoc = QJsonDocument::fromJson(replyText, &parseError);

    if (parseError.error != QJsonParseError::NoError) {
        qWarning() << "reply:" << replyText << "error:" << parseError.errorString();

        return QJsonObject();
    }

    QJsonObject json;
    if (jsonDoc.isObject())
        json = jsonDoc.object();

    return json;
}
