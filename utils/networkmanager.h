#pragma once

#include <functional>
#include <QObject>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>
#include <QJsonObject>
#include <QSharedPointer>
#include <QString>
#include <QVariantMap>

class NetworkManager : public QObject
{
    using onCompleteFunc = std::function<void(const QJsonObject &, const QList<QNetworkReply::RawHeaderPair> &)>;
    using onErrorFunc = std::function<void(const QString &)>;

public:
    NetworkManager();

    void sendRequest(const QString &apiStr, const onCompleteFunc &onCompleteCallback, const onErrorFunc &onErrorCallback, const QVariantMap &data);

private:
    QNetworkRequest createRequest(const QString &apiStr) const;
    bool isFinishedWithoutError(QNetworkReply *reply) const;
    QJsonObject parseReply(QNetworkReply *reply) const;

private:
    QSharedPointer<QNetworkAccessManager> m_restclient;

    QString m_server;
    int m_port;
};
